<?php
header('Content-Type: text/plain');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $USER;
$start = microtime(true);
$pass = uniqid();
// Собираем прикрепленные файлы

$arAttachFields = array();
if($_FILES) {
    foreach ($_FILES as $id => $file) {
        $arAttachFields[] = array('file' => curl_file_create($_FILES[$id]['tmp_name'], $_FILES[$id]['type'], $_FILES[$id]['name']));
    }
}

$error = '';

// Создаем токены для файлов

if($arAttachFields) {
    foreach ($arAttachFields as $attach) {
        $objCurl = curl_init();
        curl_setopt_array($objCurl, array(
            CURLOPT_URL => 'https://rospartner.intraservice.ru/api/TaskFile',
            CURLOPT_USERPWD => "login:pass",
            CURLOPT_HTTPHEADER => array('Content-Type: multipart/form-data'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_VERBOSE => true,
            CURLOPT_POSTFIELDS => $attach
        ));
        $result = curl_exec($objCurl);

        curl_close($objCurl);
        $res = json_decode($result);

		if (property_exists($res, 'Message')) {
			$error = $res->Message;
			//file_put_contents($_SERVER["DOCUMENT_ROOT"].'/ajax/support.txt', $error."\n", FILE_APPEND);
		} else if (property_exists($res, 'FileTokens')) {
			$filetokens[] = $res->FileTokens;
			//file_put_contents($_SERVER["DOCUMENT_ROOT"].'/ajax/support.txt', $res->FileTokens."\n", FILE_APPEND);
		}
    }
}


if ($error != '') {
	//Возникла ошибка с прилагаемыми файлами
	die(json_encode(array('error' => $error)));
}

if($filetokens) {
    foreach ($filetokens as $id => $filetoken) {
        if ($id == 0) {
            $idtokens .= $filetoken;
        } else {
            $idtokens .= "," . $filetoken;
        }
    }
}

//file_put_contents(dirname(__FILE__).'/gettask.txt', '2 - '.(microtime(true) - $start)."\n", FILE_APPEND);

// Обработка запроса на создание обращения
if($_REQUEST["theme"]){
    $get = array(
        'http' => array(
            'header'  => "Authorization: Basic " . base64_encode("login:pass") . "\r\nContent-type: application/x-www-form-urlencoded",
            'method'  => "GET",
        )
    );
    $contextget = stream_context_create($get);
    $ID = $USER->GetID();
    $filter = Array("ID" => $ID);
    $select = array("SELECT" => array("UF_*"));

    // Получаем данные текущего пользователя

    $rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter, $select);
    if ($arUser = $rsUsers->Fetch()) {
        $login = $arUser["LOGIN"];
        $name = $arUser['NAME'];
        $last_name = $arUser['LAST_NAME'];
        $second_name = $arUser['SECOND_NAME'];
        $email = $arUser['EMAIL'];
        $phone = $arUser['WORK_PHONE'];
        $comments = $arUser['UF_COUNT_COMMENT'];
        $company = $arUser['UF_COMPANY_NAME'];
        $file = CFile::ResizeImageGet($arUser["UF_PHOTO"], array('width'=>60, 'height'=>60), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $photo = $file["src"];
    }
    if(strlen($name.$last_name.$second_name) >0){
        $fio = trim($name." ".$last_name." ".$second_name);
    }
    else{
        $fio = $login;
    }

    // Получаем информацию о заявках пользователя по его почтовому адресу

    $getusers = file_get_contents('https://rospartner.intraservice.ru/api/user?email='.$email,false, $contextget);
    $getcontusers = json_decode($getusers);
    if($getcontusers->Users) {
        foreach ($getcontusers->Users as $user) {
            if ($user->Email == $email) {
                $intrauserid = $user->Id;
            }
        }
    }

    // Если такого пользователя нет, то создаем

    if(!$intrauserid){
        $data = array(
            'RoleId'=>"54",
            'CompanyId'=>"97",
            'Login'=>$email,
            'Name'=>$fio,
            'Password'=>$pass,
            'ConfirmPassword'=>$pass,
            'Email'=>$email,
            'Phone'=>$phone,
        );
        $data = json_encode($data);
        $post = array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode("login:pass") . "\r\nContent-type: application/json\r\n",
                'method'  => "POST",
                'content' => $data
            )
        );
        $contextpost = stream_context_create($post);
        $postcont = file_get_contents('https://rospartner.intraservice.ru/api/user',false, $contextpost);
        $postcont = json_decode($postcont);
        $intrauserid = $postcont->Id;
    }

    // Добавляем пользователя в сервис

    $data = array(
        'ServiceId'=>26,
        'AddUsers'=>array(array('UserId'=> $intrauserid, 'RoleId'=> 54))
    );
    $data = json_encode($data);
    $post = array(
        'http' => array(
            'header'  => "Authorization: Basic " . base64_encode("login:pass") . "\r\nContent-type: application/json\r\n",
            'method'  => "POST",
            'content' => $data
        )
    );
    $contextpost = stream_context_create($post);
    $postcont = file_get_contents('https://rospartner.intraservice.ru/api/serviceuser',false, $contextpost);

	$Name = trim(htmlspecialchars(strip_tags(str_replace(array("\n", "\r"), "", $_REQUEST["theme"]))));
	$Description = trim(htmlspecialchars(strip_tags(str_replace(array("\n", "\r"), "", $_REQUEST["answer"]))));
	//$Description = preg_replace('/<[^>]+>(.*)<\/[^>]+>/Ui', '', $_REQUEST["answer"]);

    // Создаем обращение

    $data = array(
        'Name' => $Name,
        'ServiceId' => '26',
        'StatusId' => '31',
        'PriorityId' => '9',
        'TypeId' => '1004',
        'FileTokens' => $idtokens,
        'UserRoleId' => '54',
        'UserCompanyId' => '97',
        'UserName' => $fio,
        'UserPassword' => $pass,
        'UserConfirmPassword' => $pass,
        'UserEmail' => $email,
        'UserPhone' => $phone,
        'Description' => $Description
    );

    $data = json_encode($data);

    $post = array(
        'http' => array(
            'header'  => "Authorization: Basic " . base64_encode("login:pass") . "\r\nContent-type: application/json\r\n",
            'method'  => "POST",
            'content' => $data
        )
    );
    $contextpost = stream_context_create($post);
    $postcont = @file_get_contents('https://rospartner.intraservice.ru/api/task',false, $contextpost);
    $postcont = json_decode($postcont);

	if (!is_object($postcont) || !property_exists($postcont, 'Task')) {
		die(json_encode(array('error' => 'Обращение не было сохранено. Проверьте правильность введённых данных.')));
	}

    $_REQUEST["id"] = $postcont->Task->Id;;
    $_REQUEST["name"] = $postcont->Task->Name;
    $_REQUEST["desc"] = $postcont->Task->Description;

	/*ob_start();
	echo '<pre>'; print_r($postcont); echo '</pre>';
	file_put_contents(dirname(__FILE__).'/gettask.txt', ob_get_clean()."\n", FILE_APPEND);*/
}


// Обработка запроса на создание комментария
if($_REQUEST["comment"]){
    $data = array(
        'Comment' => strip_tags($_REQUEST["comment"]),
        'FileTokens' => $idtokens,
		//'ParentId' => $_REQUEST["id"]
    );
    
	$data = json_encode($data);
	
    $put = array(
        'http' => array(
            'header'  => "Authorization: Basic " . base64_encode("login:pass") . "\r\nContent-type: application/json\r\n",
            'method'  => "PUT",
            'content' => $data
        )
    );


    $contextput = stream_context_create($put);

	$gettasks = file_get_contents('https://rospartner.intraservice.ru/api/task/'.$_REQUEST["id"], false, $contextput);

    $getconttasks = json_decode($gettasks);
}

$get = array(
    'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("login:pass") . "\r\nContent-type: application/x-www-form-urlencoded",
        'method'  => "GET",
    )
);
$contextget = stream_context_create($get);

$getcont = @file_get_contents('https://rospartner.intraservice.ru/api/tasklifetime?taskid='.$_REQUEST["id"], false, $contextget);
$getcontq = json_decode($getcont);

ob_start();

?>
<header class="split-view__header">
    <h1><?=$_REQUEST["name"]?></h1>
    <span class="support-info__id" id="<?=$_REQUEST["id"]?>">ID <?=$_REQUEST["id"]?></span>
</header>

<div class="support-info__history support-history" id="<?=$_REQUEST["name"]?>">
	<div class="preloader-circle">
		<div class="loader"></div>
	</div>
	<div class="support-history__message">
        <div class="support-history__avatar"><img src="<?if($_REQUEST["photo"]){echo $_REQUEST["photo"];}else{echo SITE_TEMPLATE_PATH."/images/avatar.png";}?>" alt=""></div>
        <div class="support-history__bubble">
            <div class="support-history__author">
                <div class="support-history__name"><?=$_REQUEST["fio"]?></div>
                <div class="support-history__sub-name"><?=$_REQUEST["company"]?></div>
            </div>
            <div class="support-history__quote">
                <?=$_REQUEST['desc']?>
            </div>
        </div>
    </div>
<?php
$i=0;


if($getcontq->TaskLifetimes) {
    $reversed = array_reverse($getcontq->TaskLifetimes);
    // Проходим циклом по комментариям к заявке и создаем блоки с комментариями
    foreach ($reversed as $id=>$comment){
        $date = str_ireplace("T", " ", $comment->Date);
        $timecreate = date("m d Y H:i", strtotime($date));
        $arDATE = ParseDateTime($timecreate, FORMAT_DATETIME);
        $datecreate = trim($arDATE["DD"] . " " . ToLower(GetMessage("MONTH_" . intval($arDATE["MM"]) . "_S")) . " " . $arDATE["YYYY"] . " " . $arDATE["H"] . ":" . $arDATE["MI"]);
        if($comment->Comments) {
            if ($comment->EditorId !== 75) {
                $i++;
            }
            ?>
            <div class="support-history__message<?
            if ($comment->EditorId !== 75) {
                echo " support-history__message--support";
            } ?>">
                <div class="support-history__avatar"><img src="<?
                    if ($comment->EditorId == 75) {
                        if($_REQUEST["photo"]){
                            echo $_REQUEST["photo"];
                        }
                        else{
                            echo SITE_TEMPLATE_PATH."/images/avatar.png";
                        }
                    } else {
                        echo SITE_TEMPLATE_PATH . "/images/image-user1.jpg";
                    } ?>" alt=""></div>

                <div class="support-history__bubble">
                    <div class="support-history__author">
                        <div class="support-history__name"><?
                            if ($comment->EditorId == 75) {
                                echo $_REQUEST["fio"];
                            } else {
                                echo $comment->Editor;
                            } ?></div>
                        <div class="support-history__sub-name"><?
                            if ($comment->EditorId == 75) {
                                echo $_REQUEST["company"];
                            } else {
                                echo "Служба технической поддержки";
                            } ?></div>
                    </div>
                    <div class="support-history__quote">
                        <?php echo $comment->Comments; ?>
                    </div>
                    <div class="support-history__time"><?= $datecreate ?></div>
<?php
					if ($comment->AddedFiles) {
						$files = explode(',', $comment->AddedFiles);

						foreach ($files as $file) {
							$parts = array_map('trim', explode('|', $file));
							if (is_array($parts) && (count($parts) == 2)) {
								$file = $parts[0];
								$name = $parts[1];
	
								if ($file && $name) {
									$dir = $_SERVER["DOCUMENT_ROOT"].'/upload/support/'.$file;
									
									if (!file_exists($dir.'/'.$name)) {
										$getcont2 = file_get_contents('https://rospartner.intraservice.ru/api/taskfile/'.$file, false, $contextget);
										
										if (!is_dir($dir)) {
											mkdir($dir);
											chmod($dir, 0777);
										}
										
										$handle = fopen($dir.'/'.$name, "w+");
										fwrite($handle, $getcont2);
										fclose($handle);
									}
	
									if (file_exists($dir.'/'.$name)) {
										echo '<div class="support-history__file">Прикреплённый файл <a href="/upload/support/'.$file.'/'.$name.'" target="_blank">'.$name.'</a></div>';
									}
								}
							}
						}
					}
?>
                </div>
            </div>
<?php
        }
    }
}

$ID = $USER->GetID();

// Получаем свойство с комментариями пользователя и добавляем туда новые

$filter = Array("ID" => $ID);
$select = array("SELECT" => array("UF_*"));
$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter, $select); // выбираем пользователей
if ($arUser = $rsUsers->Fetch()) {
    $fields["UF_COUNT_COMMENT"] = $arUser["UF_COUNT_COMMENT"];
}
$user = new CUser;
$find = "N";
if($fields["UF_COUNT_COMMENT"]) {
    foreach ($fields["UF_COUNT_COMMENT"] as $num => $field) {
        $tid = explode("|", $field)[0];
        if ($tid == $_REQUEST["id"]) {
            $fields["UF_COUNT_COMMENT"][$num] = $_REQUEST["id"] . "|" . $i;
            $find = "Y";
        }
    }
}
if($find == "N"){
    $fields["UF_COUNT_COMMENT"][] = $_REQUEST["id"]."|".$i;
}
$user->Update($ID, $fields);

?>
</div>
<?php
	/* Проверить статус заявки
	26 - Отложена
	27 - В работе
	28 - Закрыта
	29 - Выполнена
	30 - Отменена
	31 - Зарегистрирована
	35 - Требует уточнения
	36 - Согласование
	37 - Во внешней организации
	38 - Требует уточнения (В работе)
	40 - Анализ
	41 - Открыта
	42 - На доработке
	*/
	$getcont = file_get_contents('https://rospartner.intraservice.ru/api/task?Id='.$_REQUEST["id"].'&fields=StatusId',false, $contextget);
    $getcontq = json_decode($getcont);
	
	if (!in_array($getcontq->Task->StatusId, array(28))) {
?>
<form class="support-info__new-message support-form" enctype="multipart/form-data" method="post" action="" id="newcomm_form">
    <label class="textarea-container">
        <textarea name="comment" type="text" placeholder="1" required></textarea>
        <span>Добавить еще комментарий</span>
		<div class="input-container__indicator"></div>
		<div class="error-message">
			<ul class="error-message__list">
				<li class="error-message__item">
					<span>Ошибка</span>
				</li>
			</ul>
		</div>
    </label>

    <div class="support-form__footer">
        <label class="send-file input-file drop-zone">
            <input type="file" class="visually-hidden file" name="file" multiple>
            <div class="drop-zone__action">
				<div class="input-file__name">Прикрепить файл</div>
				<output class="input-file__list"></output>
			</div>
			<div class="error-message">
				<ul class="error-message__list">
					<li class="error-message__item">
						<span>Ошибка</span>
					</li>
				</ul>
			</div>
        </label>
        <button type="button" class="button button--primary" onclick="suportMessenger('')" id="newcomm">Отправить</button>
    </div>
</form>
<?php } ?>
<script>
var task = document.getElementById('<?=$_REQUEST["id"]?>');
$(task).find(".support-ticket__count").remove();
new ElemSelect('drop-zone');
</script>
<?php

die(json_encode(array('html' => str_replace("\n", '', ob_get_clean()))));
?>