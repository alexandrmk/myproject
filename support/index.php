<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
global $USER;
$ID = $USER->GetID();
$filter = Array("ID" => $ID);
$select = array("SELECT" => array("UF_*"));

// Получаем данные текущего пользователя

$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter, $select);
if ($arUser = $rsUsers->Fetch()) {
    $login = $arUser["LOGIN"];
    $name = $arUser['NAME'];
    $last_name = $arUser['LAST_NAME'];
    $second_name = $arUser['SECOND_NAME'];
    $email = $arUser['EMAIL'];
    $phone = $arUser['WORK_PHONE'];
    $comments = $arUser['UF_COUNT_COMMENT'];
    $company = $arUser['UF_COMPANY_NAME'];
    $file = CFile::ResizeImageGet($arUser["UF_PHOTO"], array('width'=>120, 'height'=>120), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    $photo = $file["src"];
}

if(strlen($name.$last_name.$second_name) >0){
    $fio = trim($name." ".$last_name." ".$second_name);
}
else{
    $fio = $login;
}



$get = array(
    'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("login:pass") . "\r\nContent-type: application/x-www-form-urlencoded",
        'method'  => "GET",
    )
);
$contextget = stream_context_create($get);
$pass = uniqid();

// Получаем информацию о заявках пользователя по его почтовому адресу

$getusers = file_get_contents('https://rospartner.intraservice.ru/api/user?email='.$email,false, $contextget);
$getcontusers = json_decode($getusers);

if($getcontusers->Users) {
    foreach ($getcontusers->Users as $user) {
        if ($user->Email == $email) {
            $intrauserid = $user->Id;

	    $url = 'https://rospartner.intraservice.ru/api/task?CreatorIds='.$intrauserid;

            $getcont = file_get_contents($url,false, $contextget);
            $getcontq = json_decode($getcont);

        }
    }
}
?>
    <script>
        var descs = [];
        var names = [];
    </script>
<?php



if($getcontq->Tasks) {
    // Комментарии к задаче, сохраненные в свойствах пользователя
    if($comments) {
        foreach ($comments as $comment) {
            $tid = explode("|", $comment)[0];
            $count = explode("|", $comment)[1];
            $comms[$tid] = $count;
        }
    }
    ?>
    <div class="page page--support page--split-view">
		<div class="split-view container">
			<section class="split-view__list">
				<header class="split-view__header">
					<h1>Мои обращения</h1>
					<div class="split-view__preview-item split-view__preview-item--active split-view__preview-item--new">
						<a class="split-view__item-link" href="javascript:void(0);" title="Создать новое обращение">
							<span class="support-ticket__title">
								<i class="material-icons">add</i>
								<span>Задать вопрос</span>
							</span>
						</a>
					</div>
				</header>

				<nav class="split-view__navigation <?php if (count($getcontq->Tasks) == 0) { ?>split-view__navigation--empty custom-scroll<?php } else { ?>split-view__navigation--support split-view__navigation--open custom-scroll<?php } ?>">
					<ul id="tasklist">
<?php

    // Проходимся циклом по задачам

    foreach ($getcontq->Tasks as $id=>$task) {

        // Вычисляем актуальное количество комментариев от техподдержки

        $getcont = file_get_contents('https://rospartner.intraservice.ru/api/tasklifetime?taskid='.$task->Id,false, $contextget);
        $getcontq = json_decode($getcont);
        $i=0;
        foreach ($getcontq->TaskLifetimes as $id=>$comm){
            if(($comm->Comments) && ($comm->EditorId !== 75)) {
                $i++;
            }
        }
        //Есть ли новые комментарии к заявке
        if($comms[$task->Id]) {
            $newcount = $comms[$task->Id];
        }
        else{
            $newcount = 0;
        }
        $new = $i - $newcount; // Количество новых комментариев
        $date = str_ireplace("T", " ", $task->Created);
        $timecreate = date("m d Y H:i", strtotime($date));
        $arDATE = ParseDateTime($timecreate, FORMAT_DATETIME);
        $datecreate = trim($arDATE["DD"] . " " . ToLower(GetMessage("MONTH_" . intval($arDATE["MM"]) . "_S")) . " " . $arDATE["YYYY"]." ".$arDATE["H"].":".$arDATE["MI"]);
        $tasks[$task->Id] = $task->Description;
?>
						<script>
                            // Заносим в массивы описание и название заявки
							descs.push({'<?php echo $task->Id?>':'<?php echo str_replace(array("\n", "\r"), '', $task->Description); ?>'});
							names.push({'<?php echo $task->Id?>':'<?php echo $task->Name?>'});
						</script>
						<li class="split-view__preview-item support-ticket<?if ( $task->StatusId == 29) { echo " support-ticket--decided"; } if($new>0) { ?> support-ticket--new<? } ?>" id="<?=$task->Id?>">
							<a class="split-view__item-link" href="javascript:void(0);" title="Выбрать тикет">
								<span class="support-ticket__id">ID: <?=$task->Id?></span>
								<span class="support-ticket__title"><?=$task->Name?></span>
								<span class="support-ticket__date"><?=$datecreate?></span>
								<? if($new>0) { ?>
								<span class="support-ticket__count"><?=$new?></span>
								<? } ?>
							</a>
						</li><?
    }
?>
    				</ul>
    			</nav>
    		</section>
			<section class="split-view__detail custom-scroll support-info">
                <div class="preloader-circle">
                    <div class="loader"></div>
                </div>
				<header class="split-view__header">
					<h1>Новое обращение в техническую поддержку</h1>
				</header>
				<form class="support-info__new-message support-form" enctype="multipart/form-data" method="post" action="" id="newtask_form">
					<label class="input-container">
						<input name="theme" type="text" placeholder="1" required>
						<span>Тема вопроса</span>
						<div class="input-container__indicator"></div>
						<div class="error-message">
							<ul class="error-message__list">
								<li class="error-message__item">
									<span>Заполните, пожалуйста, тему.</span>
								</li>
							</ul>
						</div>
					</label>
		
					<label class="textarea-container">
						<textarea class="input" name="answer" type="text" placeholder="1" required></textarea>
						<span>Задайте вопрос</span>
						<div class="input-container__indicator"></div>
						<div class="error-message">
							<ul class="error-message__list">
								<li class="error-message__item">
									<span>Ошибка</span>
								</li>
							</ul>
						</div>
					</label>
		
					<div class="support-form__footer">
						<label class="send-file input-file drop-zone">
							<input type="file" class="visually-hidden file" name="file" id="taskfile" multiple>
							<div class="drop-zone__action">
								<div class="input-file__name">Прикрепить файл</div>
								<output class="input-file__list"></output>
							</div>
							<div class="error-message">
								<ul class="error-message__list">
									<li class="error-message__item">
										<span>Ошибка</span>
									</li>
								</ul>
							</div>
						</label>
						<button type="button" id="newcomm" class="button button--primary" onclick="suportMessenger('newtask')" id="newtask">Отправить</button>
					</div>
				</form>
			</section>
		</div>
		<div class="faq-container">
			<button class="button button-faq--hide">Часто задаваемые вопросы </button>
			<ul class="faq-container__list">
				<li><a href="#">Интеграция с Allure</a></li>
				<li><a href="#">Модель интеграции</a></li>
				<li><a href="#">Состояния проекта</a></li>
				<li><a href="#">ETL-приемник FM SM</a></li>
				<li><a href="#">Генератор событий</a></li>
				<li><a href="#">SM Action Generator</a></li>
				<li><a href="#">Состояния проекта</a></li>
			</ul>
		</div>
    </div>
<?
} else {
?>
    <div class="page page--support page--split-view">
        <div class="split-view container">
            <section class="split-view__list">
                <header class="split-view__header">
                    <h1>Мои обращения</h1>
                    <div class="split-view__preview-item split-view__preview-item--active split-view__preview-item--new">
                        <a class="split-view__item-link" href="javascript:void(0);" title="Создать новое обращение">
                            <span class="support-ticket__title">
                                <i class="material-icons">add</i>
                                <span>Задать вопрос</span>
                            </span>
                        </a>
                    </div>
                </header>
                <nav class="split-view__navigation split-view__navigation--empty custom-scroll">
					<em>История обращений в поддержку отсутствует</em><?php /* ?>
					<!-- Когда нет обращений: добавляем класс-модификатор к списку, справа показываем создание нового обращения --><?php */ ?>
                </nav>
            </section>

            <section class="split-view__detail custom-scroll support-info">
                <div class="preloader-circle">
                    <div class="loader"></div>
                </div>
                <header class="split-view__header">
                    <h1>Новое обращение в техническую поддержку</h1>
                </header>

                <form class="support-info__new-message support-form" enctype="multipart/form-data" method="post" action="" id="newtask_form">
                    <label class="input-container">
                        <input name="theme" type="text" placeholder="1" required>
                        <span>Тема вопроса</span>
						<div class="input-container__indicator"></div>
                        <div class="error-message">
                            <ul class="error-message__list">
                                <li class="error-message__item">
                                    <span>Ошибка</span>
                                </li>
                            </ul>
                        </div>
                    </label>

                    <label class="textarea-container">
                        <textarea class="input" name="answer" type="text" placeholder="1" required></textarea>
                        <span>Задайте вопрос</span>
						<div class="input-container__indicator"></div>
                        <div class="error-message">
                            <ul class="error-message__list">
                                <li class="error-message__item">
                                    <span>Ошибка</span>
                                </li>
                            </ul>
                        </div>
                    </label>

                    <div class="support-form__footer">
                        <label class="send-file input-file drop-zone">
                            <input type="file" class="visually-hidden file" name="file" id="taskfile" multiple>
							<div class="drop-zone__action">
								<div class="input-file__name">Прикрепить файл</div>
								<output class="input-file__list"></output>
							</div>
							<div class="error-message">
								<ul class="error-message__list">
									<li class="error-message__item">
										<span>Ошибка</span>
									</li>
								</ul>
							</div>
                        </label>
                        <button type="button" class="button button--primary" onclick="suportMessenger('newtask')" id="newtask">Отправить</button>
                    </div>
                </form>
            </section>
        </div>
        <div class="faq-container">
            <button class="button button-faq--hide">Часто задаваемые вопросы</button>
            <ul class="faq-container__list">
                <li><a href="#">Интеграция с Allure</a></li>
                <li><a href="#">Модель интеграции</a></li>
                <li><a href="#">Состояния проекта</a></li>
                <li><a href="#">ETL-приемник FM SM</a></li>
                <li><a href="#">Генератор событий</a></li>
                <li><a href="#">SM Action Generator</a></li>
                <li><a href="#">Состояния проекта</a></li>
            </ul>
        </div>
    </div>
    <?
}
?>
<script type="text/javascript">
<?php
if($_REQUEST["page"] == "main"){
?>
    var id = '<?=$_REQUEST["id"]?>';
    $("[class='split-view__preview-item split-view__preview-item--new split-view__preview-item--active']").removeClass("split-view__preview-item--active");
    $("[id="+id+"]").addClass("split-view__preview-item--active");
    var desc = descs.find(x => x[id])[id];
    var name = names.find(x => x[id])[id];
    $.ajax({
        url: '/ajax/gettask.php',
        data: {"id": id, "fio": '<?=$fio?>', "photo": '<?=$photo?>', "desc": desc, "name": name},
        cache: false,
        success: function(data){
			if (typeof(data.error) != 'undefined') {
				alert(data.error);
			} else if (typeof(data.html) != 'undefined') {
            	$(".split-view__detail").empty();
            	$(".split-view__detail").append(data.html);
			}
        }
    });
<?
}
?>
    var taskblock = document.getElementsByClassName('split-view__list');
// Получение информации о задаче по клику на нее
    $(taskblock).on("click", ".split-view__preview-item.support-ticket", function (e) {
console.log('taskblock click1');
        var id = $(this).attr("id");
		$(".split-view__preview-item--active").removeClass("split-view__preview-item--active");<?php /* ?>
        $("[class='split-view__preview-item support-ticket split-view__preview-item--active']").removeClass("split-view__preview-item--active");
        $("[class='split-view__preview-item support-ticket support-ticket--decided split-view__preview-item--active']").removeClass("split-view__preview-item--active");
		$("[class='split-view__preview-item split-view__preview-item--new split-view__preview-item--active']").removeClass("split-view__preview-item--active");<?php */ ?>

        $(this).addClass("split-view__preview-item--active");
console.log(id);
        var desc = descs.find(x => x[id])[id];
        var name = names.find(x => x[id])[id];

		if ($('.support-info__history').length == 1) $('.support-info__history').addClass('support-history--loading');

        $.ajax({
            url: '/ajax/gettask.php',
            data: {"id": id, "fio": '<?=$fio?>', "photo": '<?=$photo?>', "desc": desc, "name": name, "company": '<?=$company?>'},
            cache: false,
			dataType: 'json',
            success: function(data){
				if (typeof(data.error) != 'undefined') {
					alert(data.error);
				} else if (typeof(data.html) != 'undefined') {
					$('.support-info__history').removeClass('support-history--loading');
	
					$(".split-view__detail").empty();
					$(".split-view__detail").append(data.html);
				}
        	}
        });
    });

    var taskbutton = document.getElementsByClassName('split-view__detail');
	// Функция отправки сообщения и создания нового  обращения
    var newTask = function () {

        var taskfiles = new FormData();
        for (var index = 0; index < fileStorage.length; index++) {
			taskfiles.append(index, fileStorage[index]);
        }

		if ($('.support-info__history.support-history').length)
        	$('.support-info__history.support-history').addClass('support-history--loading');
		else
			$('.split-view__detail.custom-scroll.support-info').addClass('support-history--loading');

        var theme = $("[name=theme]").val();
		theme = $.trim($('<div>' + theme + '</div>').text());

        var answer = $("[name=answer]").val();
		answer = $.trim($('<div>' + answer + '</div>').text());

        taskfiles.append("theme", theme);
        taskfiles.append("answer", answer);
        taskfiles.append("fio", '<?=$fio?>');
        taskfiles.append("photo", '<?=$photo?>');
        taskfiles.append("company", '<?=$company?>');
        // Запрос на создание обращения и получение информации о нем
        $.ajax({
            url: '/ajax/gettask.php',
            type: "POST",
            data: taskfiles,
			dataType: 'json',
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false
        }).done(function(data) {
			if (typeof(data.error) != 'undefined') {
				$('.support-history--loading').removeClass('support-history--loading');
				alert(data.error);
			} else if (typeof(data.html) != 'undefined') {
				var html = data.html;
                $("[class='split-view__preview-item split-view__preview-item--new split-view__preview-item--active']").removeClass("split-view__preview-item--active");
				$(".split-view__detail").empty();
				$(".split-view__detail").append(html);

				var taskid = $("[class=support-info__id]").attr('id');
				var tasktheme = $("[class='support-info__history support-history']").attr('id');
				var taskdate = $("[class=support-history__time]").text();
				var taskdesc = $("[class=support-history__quote]").text();

				if($("[id=tasklist]").length == 0){
					var list = '<ul id="tasklist">\n' + '</ul>\n';
					$(".split-view__navigation.split-view__navigation--empty.custom-scroll").html(list);
				}

				$('.support-info__history.support-history').removeClass('support-history--loading');
				$('.split-view__detail.custom-scroll.support-info').removeClass('support-history--loading');

				var field = '<li class=\"split-view__preview-item support-ticket split-view__preview-item--active\" id=\"'+taskid+'\">\n' +
					'                    <a class="split-view__item-link" href="javascript:void(0);" title="Выбрать тикет">\n' +
					'                <span class="support-ticket__id">ID: '+taskid+'</span>\n' +
					'<span class="support-ticket__title">'+tasktheme+'</span>\n' +
					'                    <span class="support-ticket__date">'+taskdate+'</span>\n' +
					'                </a>\n' +
					'                </li>\n';
				$("[id=tasklist]").prepend(field);
	
				var tdesc = {};
				var ttheme = {};
				tdesc[taskid] = taskdesc;
				ttheme[taskid] = tasktheme;
				descs.push(tdesc);
				names.push(ttheme);
				store={};
				fileStorage = [];
				new ElemSelect('drop-zone');
				if ($('.split-view__navigation').hasClass('split-view__navigation--empty')) $('.split-view__navigation').removeClass('split-view__navigation--empty');
			}
        });
    };
    // Функция отправки сообщения и создания нового  обращения end.


    // Функция отправки сообщения и создания нового  комментария
    var newMessage = function () {
        var commfiles = new FormData();

        for (var index = 0; index < fileStorage.length; index++) {
            commfiles.append(index, fileStorage[index]);
        }

		if ($("[class=support-info__id]").length > 0) {
			var btn = $('#newcomm');
			btn.attr('disabled', 'disabled');

        	var id = $("[class=support-info__id]").attr('id');
        	var comment = $("[name=comment]").val();
			comment = $.trim($('<div>' + comment + '</div>').text());

        	var desc = descs.find(x => x[id])[id];
        	var name = names.find(x => x[id])[id];

			$("[class='split-view__preview-item split-view__preview-item--new split-view__preview-item--active']").removeClass("split-view__preview-item--active");

			if ($('.support-info__history.support-history').length)
				$('.support-info__history.support-history').addClass('support-history--loading');
			else
				$('.split-view__detail.custom-scroll.support-info').addClass('support-history--loading');

			commfiles.append("id", id);
			commfiles.append("desc", desc);
			commfiles.append("name", name);
			commfiles.append("comment", comment);
			commfiles.append("fio", '<?=$fio?>');
			commfiles.append("photo", '<?=$photo?>');
			commfiles.append("company", '<?=$company?>');

            // Запрос на создание комментария и получение информации о нем
			$.ajax({
				url: '/ajax/gettask.php',
				type: "POST",
				data: commfiles,
				dataType: 'json',
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false
			}).done(function(data) {
				btn.removeAttr('disabled');

				if (typeof(data.error) != 'undefined') {
					$('.support-history--loading').removeClass('support-history--loading');
					alert(data.error);
				} else if (typeof(data.html) != 'undefined') {
					var html = data.html;

					$(".split-view__detail").empty();
					$(".split-view__detail").append(html);
					store={};
					fileStorage = [];
                    $("[class='support-info__history support-history support-history--loading']").removeClass("support-history--loading");
				}
			});
		} else {
			newTask();
		}
    };
    // Функция отправки сообщения и создания нового комментария end.
</script>
<style type="text/css">
.drop-zone__action {
	cursor: pointer;
}
</style>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>